var authcontroller = angular.module('authServices',[])

.controller('loginCtrl',function($http,$location,$timeout,Auth,Token){
	var app = this;
	app.user= {username:'',password:''}	
	
	
	app.login = function(){
		
		
			Auth.login(app.user).then(function(data){
				if(data.data.success){
					console.log(data)
					Token.setToken(data.data.token)
					app.message ="success"
					$timeout(function(){
					$location.path('/main')

					},2000)
					
				}else{
						app.message =data.data.message
				}
				
			});
		

		
	}
	
	
})

.controller('regCtrl',function($rootScope,$location,$http,$timeout,Auth,Token){
	var reg = this;
	reg.user = {username:'',no:'',email:'',password:''};
	reg.Register = function(){
		$rootScope.justRegistered = true;
		Auth.register(reg.user).then(function(data){
			
			if(data.data.success){
				reg.message = data.data.message;
				Token.setToken(data.data.token)
				$timeout(function(){
					$location.path('/main')
				},2000)
				

			}else{
				reg.message = data.data.message;
			}

			
		})
		
	}
	
})