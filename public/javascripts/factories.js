var app = angular.module('factories',[])


.factory('Auth',function(Token,$http){
	var authfact ={};
	var control = '';
	authfact.getUser = function(){
		return $http.post('/users/current');
		
	};
	authfact.IsloggedIn = function(){

		if(Token.getToken() ==="undefined"){
			return false;
		}
		else{
			
			return true;
		}
		
	}
	authfact.register = function(user){
		return $http.post('/users/register',user);
	}
	authfact.login = function(user){
		return $http.post('/users/login',user);
	}
	authfact.logout = function(user){
		Token.setToken();
	}

	return authfact;
})
.factory('Token',function($window){
	var tokenfact = {}
	tokenfact.setToken = function(token) {
		$window.localStorage.setItem('token',token);
	}
	tokenfact.getToken =function(){
		
		return $window.localStorage.getItem('token'); 
	}
	return tokenfact;
})
.factory('Interceptors',function(Token){
	var	Ifact = {}
	Ifact.request =function(config){

		var token = Token.getToken()
		if(token)
			config.headers['x-access-token'] = token;
		return config
	}

	return Ifact;
})