var hobby = angular.module('main',['ngRoute','authServices','factories'])

.run(function(Token,Auth,$location,$rootScope){
	$rootScope.justRegistered = false;
	//on every route do this
	$rootScope.$on( "$routeChangeStart", function(event, next, current) {
      Auth.getUser().then(function(d){
      	//check if the session is over
		if(d.data.failure){
			Token.setToken(); //remove token if session is over i.e log out
			if($location.path() =="/main") $location.path('/');//stop route to main when not logged in
		}else{
			if($location.path() =="/") $location.path('/main');
			$location.path('/main')//route to main page if there's a token
		}
	})
    });
})
.config(function($routeProvider){
    $routeProvider
        .when('/register',{
        controller: 'regCtrl',
        templateUrl: 'register.html',
        controllerAs:'reg'
        
    })

        .when('/',{
        controller: 'loginCtrl',
        templateUrl: 'login.html',
        controllerAs:'auth'
        
    })
        .when('/main',{
        controller: 'mainCtrl',
        templateUrl: 'main.html',
        
        
    })
        .otherwise({redirectTo:'/'});

})
.config(function($httpProvider){
	//sure the token is set on every route
	$httpProvider.interceptors.push('Interceptors');
})

.controller('mainCtrl',function($scope ,$rootScope,$http,$location,$timeout,Auth){

	var refresh = function(user){
		$http.post('/api/hobbies',user).then(function(data){
			
				$scope.list = data.data
				
		});};
	if(Auth.IsloggedIn()){
		//get the current user logged in and set all data
		Auth.getUser().then(function(data){
			$rootScope.user = data.data;
			$rootScope.username = data.data.username;
			$scope.hobby = {name:'',username :$rootScope.username,date:''}
			$rootScope.active = Auth.IsloggedIn();
			//get hobbies for user
			refresh($scope.user)
		})}
	else{
		console.log("no user logged in");
	}
		
	$rootScope.active=Auth.IsloggedIn();
	$rootScope.logout = function(){
		Auth.logout();
		$rootScope.home ="#/";
		$timeout(function(){
			$rootScope.active = Auth.IsloggedIn();
			$location.path('/');
	},1000)}
	$rootScope.home =Auth.IsloggedIn() ? '#/main' : "#/";

	$scope.set = function(){
		$rootScope.justRegistered = false;
	}

	//delete a hobby
	$scope.Del = function(h){
		$http.post('/api/remove',h).then(function(data){
			//get hobbies
			refresh($scope.user)
		})}

	//add a hobby	
	$scope.Add= function(h){
		if(h.name){
			$scope.hobby.date = Date.now();
		$http.post('/api/hobby',h).then(function(res){
			$scope.hobby = {name:'',username:$rootScope.user.username,date:''};

			//get all hobbies for user
			refresh($scope.user);

			//send sms and email notifications
			$http.post('/users/notifications',res.data).then(function(data){
				
			})
		})
		}

	}
});