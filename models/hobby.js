var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var HobbySchema = new Schema({
	name: String,
	username: String,
	date: Date

});

module.exports = mongoose.model('Hobby', HobbySchema)