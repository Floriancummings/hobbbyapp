
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var bcrypt = require('bcrypt-nodejs');
var UserSchema = new Schema({
	username:  {type: String, required: true, lowercase:true, unique:true},
	email:  {type: String, required: true, lowercase:true, unique:true},
	no: String,
	password: {type: String, required: true},

});

UserSchema.pre('save',function (next) {
	var user = this;
	bcrypt.hash(user.password,null,null,function(err,hash){
		if(err) return next(err)
		user.password = hash;
		next();
	})
})

UserSchema.methods.comparePassword = function(pass){
	return bcrypt.compareSync(pass,this.password)
}
module.exports = mongoose.model('User', UserSchema)