var express = require('express');
var router = express.Router();
var User = require('../models/User')
var jwt = require('jsonwebtoken');
var ses = require('../notifications/ses');
var sms = require('../notifications/sms');

var secret = "temp";

router.post('/register', function(req, res, next) {
	var user = req.body;
  User.create(user,function(err,user){
  	if(err){
  		
  		res.json({success: false, message:'Resgistration failed, check input correclty'})
  	}
  	
  	else{
  		var token = jwt.sign({
  					username : user.username,email:user.email
				}, secret, { expiresIn: '24h'});
				res.json({success: true, message:"Resgistration successful.",token: token});
				msg = user.email+" "+user.no+" just registered";
				sms.send("08174561099",msg);
  	}
	res.end();
  })
});
router.post('/login', function(req, res, next) {
	req.body.username = req.body.username.toLowerCase();
	var query = User.where({username:req.body.username});
	query.findOne(function(err,user){
		if(err) return console.error(err);
		if(user){
			var pass = user.comparePassword(req.body.password);
			if(pass){
				var token = jwt.sign({
  					username : user.username,email:user.email
				}, secret, { expiresIn: '24h' });
				res.json({success: true, message:"logged in",token: token})
			}
			else{
				res.json({success: false, message:"invalid username or password"})
			}
		}else{
			res.json({success: false, message:"invalid username or password"})
		}
		res.end();
	})
});

router.use(function(req,res,next){
	var token = req.body.token || req.body.query || req.headers['x-access-token'];
	if(token){
		jwt.verify(token,secret,function(err,decoded){
			if(err){

				res.json({failure: true, message:err})
			}else{
				req.decoded = decoded;
				next();
			}
		})

	}else{
		res.json({failure: true, message:"No token"})

	}

})


router.post('/current',function(req,res,next){
	res.send(req.decoded)	

});

router.post('/notifications',function(req,res,next){
	var query = User.where({username:req.body.username});
	query.findOne(function(err,user){
		var sub = "New Hobby";
	   
			if(err) return console.error(err);
			if(user){
				var msg = "Hello "+user.username+"!<br> you just added a hobby<br><br>Thanks for using our app :)";
				var msg1 = "Hello "+user.username+"! you just added a hobby. Thanks for using our app :)";
				sms.send(user.no,msg1);
				ses.send(user.email,sub,msg)
				
				
			}else{
				res.json({success: false, message:"no user found"})
			}
			res.end();
	});
});
module.exports = router;
