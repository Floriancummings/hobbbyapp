var express = require('express');
var router = express.Router();
var Hobby = require('../models/hobby');


/*get all hobbies from database for 'this' user */
router.post('/hobbies', function(req, res, next) {
  console.log("Getting all hobbies for User "+req.body.username);

  Hobby.find({username:req.body.username})
  .exec(function(err,data){
    if(err){
      console.log(err)
    }else{
      res.json(data);
    }
  });


});

//delete a hobby
router.post('/remove',function(req,res,next){
  Hobby.findOneAndRemove({
    _id: req.body._id
  },function(err,hobby){
      if(err)
        console.log(err);
      else
        res.json(hobby)
  });
});

//create a new hobby
router.post('/hobby', function(req, res, next) {
	Hobby.create(req.body,function(err,hobby){
		if(err){
			console.log("not created");
    }
		else{
      
			res.json(hobby); 
    }

	});





});
module.exports = router;
